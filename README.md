﻿<h1 align="center">Black<i>Coffee</i></h1>
<p align="center">Este é um site de estudo e práticas em que foi utilizado uma cafeteria como tema.</p>

<br><br><br>
### Status: Em andamento.

<img src="https://img.shields.io/static/v1?label=Personal&message=Onenine&color=7159c1&style=for-the-badge&logo=ghost"/><br>
---

### Recursos

- [x] Header
- [x] Prelude
- [x] Intro / section
- [x] Products / section
- [x] Info / section
- [x] Location / section
- [x] Footer
- [x] Responsividade
- [ ] Pop-up para o carrinho
- [ ] SASS
---
