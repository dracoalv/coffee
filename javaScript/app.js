const cartButton = document.querySelector('.header__icon--bag')
const cartPopup = document.querySelector('.popup-wrapper')
const closePopup = document.querySelector('.form__close')

cartButton.addEventListener('click', () => {
        cartPopup.style.display = 'block'
    });

closePopup.addEventListener('click', () => {
    cartPopup.style.display = 'none'
});